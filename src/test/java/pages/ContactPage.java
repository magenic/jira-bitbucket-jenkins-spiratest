package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import support.PropertyHandler;

public class ContactPage extends BasePageModel
{
	/** The page url of the contact page */
	private static final String pageUrl = PropertyHandler.getWebSiteBase() + "Home/Contact";

	/** The physical address selector */
	private static final By contactHeader = By.cssSelector("#AboutTitle");

	/** The company name selector */
	private static final By companyName = By.cssSelector("#PhysicalAddress");

	/** The email contact selector */
	private static final By emailContact = By.cssSelector("#PhysicalAddress");

	/**
	 * Constructor
	 * @param testObject The selenium test object
	 */
	public ContactPage(WebDriver testObject) 
	{
		super(testObject);
	}

	/**
	 * Opens the contact page
	 * @throws InterruptedException If the thread is interrupted
	 */
	public void openContactPage() throws InterruptedException
	{
		this.testObject.get(pageUrl);
		Thread.sleep(this.driverWaitTime);
	}

	@Override
	public boolean isPageLoaded() 
	{
		return this.testObject.findElement(contactHeader).isDisplayed();
	}

}

package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import support.PropertyHandler;

public class HomePage extends BasePageModel 
{
	/** The page url */
	private static String pageUrl = PropertyHandler.getWebSiteBase();

	/** The welcome message on the homepage */
	private static By homeButton = By.cssSelector("#homeButton > a");

	/**
	 * Constructor
	 * @param testObject The selenium test object
	 */
	public HomePage(WebDriver testObject) 
	{
		super(testObject);
	}

	/**
	 * Opens the homepage
	 * @throws InterruptedException If the thread is interrupted
	 */
	public void openHomePage() throws InterruptedException
	{
		this.testObject.get(pageUrl);
		Thread.sleep(this.driverWaitTime);
	}

	@Override
	public boolean isPageLoaded()
	{
		return this.testObject.findElement(homeButton).isDisplayed();
	}
}
